import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import com.myzoo.animals.Gender;
import com.myzoo.animals.Lion;
import com.myzoo.animals.Monkey;
import com.myzoo.animals.Snake;
import com.myzoo.animals.Vaccination;
import com.myzoo.animals.Zoo;
import com.myzoo.employees.Janitor;
import com.myzoo.employees.Manager;
import com.myzoo.management.ZooManagementTool;

public class ZooManagementToolTest {

	@Test
	public void testCreateOrder() {
		Zoo zoo = new Zoo();
		zoo.setLionStock(createTestLionStock());
		zoo.setMonkeyStock(createTestMonkeyStock());
		zoo.setSnakeStock(createTestSnakeStock());
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		String order = managementTool.createOrder();
		assertEquals("2x Tasty Steak, 4x Healthy Banana, 1x Little Rat", order);
	}

	@Test
	public void testCreateVaccinationList() {
		Zoo zoo = new Zoo();
		zoo.setLionStock(createTestLionStock());
		zoo.setMonkeyStock(createTestMonkeyStock());
		zoo.setSnakeStock(createTestSnakeStock());
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		List<Vaccination> vaccinationList = managementTool.createVaccinationList();
		assertEquals(LocalDate.now().plusDays(1), vaccinationList.get(0).getDate());
		assertEquals(LocalDate.of(2020, 7, 14), vaccinationList.get(1).getDate());
		assertEquals(LocalDate.now().plusYears(1), vaccinationList.get(2).getDate());
		assertEquals(LocalDate.of(2020, 8, 30), vaccinationList.get(3).getDate());
		assertEquals(LocalDate.of(2020, 5, 15), vaccinationList.get(4).getDate());
		assertEquals(LocalDate.now().plusDays(1), vaccinationList.get(5).getDate());
		assertEquals(LocalDate.now().plusDays(1), vaccinationList.get(6).getDate());

	}

	@Test
	public void testChangeManager() {
		Zoo zoo = new Zoo();
		zoo.setManager(createTestManagerWithSeniority());
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		managementTool.changeManager(new Manager("Mikael Olafson", 27));
		assertEquals("Mikael Olafson", zoo.getManager().getName());
		assertEquals(27, zoo.getManager().getAge());
		assertEquals(0, zoo.getManager().getSeniority());
	}

	@Test
	public void testHireJanitor() {
		Zoo zoo = new Zoo();
		zoo.addJanitor(new Janitor("Johann Müller", 51));
		zoo.addJanitor(new Janitor("Marcel Baumann", 36));
		zoo.addJanitor(new Janitor("Patrick Hofer", 22));
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		managementTool.hireJanitor(new Janitor("Hans Berner", 45));
		assertEquals("Hans Berner", zoo.getEmployedJanitors().get(3).getName());
		assertEquals(45, zoo.getEmployedJanitors().get(3).getAge());
		assertEquals(0, zoo.getEmployedJanitors().get(3).getSeniority());
		assertEquals(4, zoo.getEmployedJanitors().size());
	}

	@Test
	public void testFireJanitor() {
		Zoo zoo = new Zoo();
		Janitor hardWorkingJanitorOne = new Janitor("Johann Müller", 51);
		Janitor hardWorkingJanitorTwo = new Janitor("Marcel Baumann", 36);
		Janitor lazyJanitor = new Janitor("Patrick Hofer", 22);
		zoo.addJanitor(hardWorkingJanitorOne);
		zoo.addJanitor(hardWorkingJanitorTwo);
		zoo.addJanitor(lazyJanitor);
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		managementTool.fireJanitor(lazyJanitor);
		assertFalse(zoo.getEmployedJanitors().contains(lazyJanitor));
		assertTrue(zoo.getEmployedJanitors().contains(hardWorkingJanitorOne));
		assertTrue(zoo.getEmployedJanitors().contains(hardWorkingJanitorTwo));
		assertEquals(2, zoo.getEmployedJanitors().size());
	}

	@Test
	public void testGetMonthlySalaryForJanitor() {
		Zoo zoo = new Zoo();
		zoo.setManager(createTestManagerWithSeniority());
		Janitor janitor = new Janitor("Patrick Hofer", 22);
		zoo.addJanitor(janitor);
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		assertEquals(1000, managementTool.getMonthlySalary(janitor));
	}

	@Test
	public void testGetMonthlySalaryForManager() {
		Zoo zoo = new Zoo();
		Manager manager = createTestManagerWithSeniority();
		zoo.setManager(manager);
		zoo.addJanitor(new Janitor("Patrick Hofer", 22));
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(zoo);
		assertEquals(100000, managementTool.getMonthlySalary(manager));
	}

	@Test
	public void testGet10PercentBonus() {
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(new Zoo());
		assertEquals(1100.0, managementTool.get10PercentBonus(1000));
	}

	@Test
	public void testGet20PercentBonus() {
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(new Zoo());
		assertEquals(1200.0, managementTool.get20PercentBonus(1000));
	}

	@Test
	public void testGet50PercentBonus() {
		ZooManagementTool managementTool = new ZooManagementTool();
		managementTool.setZoo(new Zoo());
		assertEquals(1500.0, managementTool.get50PercentBonus(1000));
	}

	private Manager createTestManagerWithSeniority() {
		Manager manager = new Manager("Sir James of Iverness", 63);
		manager.setSeniority(15);
		return manager;
	}

	private List<Lion> createTestLionStock() {
		List<Lion> testLionStock = new ArrayList<Lion>();
		testLionStock.add(createTestLion("Simba", Gender.MALE));
		testLionStock.add(createTestLion("Nala", Gender.FEMALE));
		return testLionStock;
	}

	private Lion createTestLion(String name, Gender gender) {
		Lion tstLion = new Lion();
		tstLion.setGender(gender);
		tstLion.setName(name);
		return tstLion;
	}

	private List<Monkey> createTestMonkeyStock() {
		List<Monkey> testMonkeyStock = new ArrayList<Monkey>();
		testMonkeyStock.add(createTestMonkey("Charlie", Gender.MALE));
		testMonkeyStock.add(createTestMonkey("Johannes", Gender.MALE));
		testMonkeyStock.add(createTestMonkey("Marlis", Gender.FEMALE));
		testMonkeyStock.add(createTestMonkey("Anna", Gender.FEMALE));
		return testMonkeyStock;
	}

	private Monkey createTestMonkey(String name, Gender gender) {
		Monkey monkey = new Monkey();
		monkey.setGender(gender);
		monkey.setName(name);
		return monkey;
	}

	private List<Snake> createTestSnakeStock() {
		List<Snake> testSnakeStock = new ArrayList<Snake>();
		testSnakeStock.add(createTestSnake());
		return testSnakeStock;
	}

	private Snake createTestSnake() {
		Snake testSnake = new Snake();
		testSnake.setGender(Gender.FEMALE);
		testSnake.setSubspecies("Python");
		testSnake.setName("Elisabeth");
		return testSnake;
	}
}
