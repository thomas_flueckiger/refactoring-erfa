package com.myzoo.animals;

public class Animal {

	private String subspecies;

	public String getSubspecies() {
		return subspecies;
	}

	public void setSubspecies(String subspecies) {
		this.subspecies = subspecies;
	}

	public FodderType getFodderType() {
		if (this instanceof Lion) {
			return FodderType.MEAT;
		} else if (this instanceof Monkey) {
			return FodderType.BANANA;
		} else if (this instanceof Snake) {
			return FodderType.RAT;
		}
		return FodderType.STRAW;
	}
}
