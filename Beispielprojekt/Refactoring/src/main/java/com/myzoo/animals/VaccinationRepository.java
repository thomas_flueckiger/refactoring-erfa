package com.myzoo.animals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VaccinationRepository {

	public List<Vaccination> getAllVAccinations() {
		List<Vaccination> vaccinations = new ArrayList<Vaccination>();
		vaccinations.add(new Vaccination("Simba", LocalDate.of(2005, 3, 15)));
		vaccinations.add(new Vaccination("Simba", LocalDate.of(2007, 3, 20)));
		vaccinations.add(new Vaccination("Simba", LocalDate.of(2006, 2, 27)));
		vaccinations.add(new Vaccination("Nala", LocalDate.of(2017, 5, 22)));
		vaccinations.add(new Vaccination("Nala", LocalDate.of(2019, 7, 14)));
		vaccinations.add(new Vaccination("Nala", LocalDate.of(2016, 4, 12)));
		vaccinations.add(new Vaccination("Charlie", LocalDate.now()));
		vaccinations.add(new Vaccination("Johannes", LocalDate.of(2019, 8, 30)));
		vaccinations.add(new Vaccination("Marlis", LocalDate.of(2019, 5, 15)));
		vaccinations.add(new Vaccination("Elisabeth", LocalDate.of(2019, 4, 29)));
		return vaccinations;
	}
}
