package com.myzoo.animals;

import java.util.ArrayList;
import java.util.List;

import com.myzoo.employees.Janitor;
import com.myzoo.employees.Manager;

public class Zoo {

	private List<Lion> lionStock;

	private List<Monkey> monkeyStock;

	private List<Snake> snakeStock;

	private Manager manager;

	private List<Janitor> employedJanitors = new ArrayList<Janitor>();

	public List<Lion> getLionStock() {
		return lionStock;
	}

	public void setLionStock(List<Lion> lionStock) {
		this.lionStock = lionStock;
	}

	public List<Monkey> getMonkeyStock() {
		return monkeyStock;
	}

	public void setMonkeyStock(List<Monkey> monkeyStock) {
		this.monkeyStock = monkeyStock;
	}

	public List<Snake> getSnakeStock() {
		return snakeStock;
	}

	public void setSnakeStock(List<Snake> snakeStock) {
		this.snakeStock = snakeStock;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public List<Janitor> getEmployedJanitors() {
		return employedJanitors;
	}

	public void addJanitor(Janitor newJanitor) {
		employedJanitors.add(newJanitor);
	}

	public void removeJanitor(Janitor janitorToRemove) {
		employedJanitors.remove(janitorToRemove);
	}
}
