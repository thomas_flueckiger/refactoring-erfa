package com.myzoo.animals;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class VaccinationService {

	VaccinationRepository repo = new VaccinationRepository();

	public LocalDate getLastVaccination(String name) {
		List<Vaccination> allVaccinations = repo.getAllVAccinations();

		List<Vaccination> filteredVaccinations = allVaccinations.stream()
				.filter(line -> name.equals(line.getAnimalName())).collect(Collectors.toList());

		return filteredVaccinations.stream().map(v -> v.getDate()).max(LocalDate::compareTo).orElse(null);

	}
}
