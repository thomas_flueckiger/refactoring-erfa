package com.myzoo.animals;

public enum FodderType {
	
	MEAT("Tasty Steak"),
	BANANA("Healthy Banana"),
	RAT("Little Rat"),
	STRAW("Boring Straw");
	
	private final String label;
	 
    private FodderType(String label) {
        this.label = label;
    }
    
    public String getLabel() {
    	return label;
    }

}
