package com.myzoo.employees;

public class Manager {

	private String name;

	private int age;

	private int seniority;

	public Manager(String name, int age) {
		this.name = name;
		this.age = age;
		seniority = 0;
	}

	public int getMonthlySalary() {
		return 100000;
	}

	public int getDailyWoringHours() {
		return 8;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSeniority() {
		return seniority;
	}

	public void setSeniority(int seniority) {
		this.seniority = seniority;
	}
}
