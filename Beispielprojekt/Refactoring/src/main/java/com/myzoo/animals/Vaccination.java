package com.myzoo.animals;

import java.time.LocalDate;

public class Vaccination {
	
	private String animalName;
	
	private LocalDate date;
	
	public Vaccination(String animalName, LocalDate date) {
		this.animalName = animalName;
		this.date = date;
	}

	public String getAnimalName() {
		return animalName;
	}

	public LocalDate getDate() {
		return date;
	}
}
