package com.myzoo.animals;

import java.time.LocalDate;

public class Snake extends Animal {

	private String name;

	private Gender gender;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate nextRequiredVaccination(LocalDate lastVaccination) {
		if (lastVaccination == null) {
			return LocalDate.now().plusDays(1);
		} else if (lastVaccination.isBefore(LocalDate.now().minusYears(1))) {
			return LocalDate.now().plusDays(1);
		}
		return lastVaccination.plusYears(1);
	}
}
