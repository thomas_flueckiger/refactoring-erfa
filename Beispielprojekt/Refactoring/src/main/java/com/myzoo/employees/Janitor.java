package com.myzoo.employees;

public class Janitor {

	private String name;

	private int age;

	private int seniority;

	public Janitor(String name, int age) {
		this.name = name;
		this.age = age;
		seniority = 0;
	}

	public int getMonthlySalary() {
		return 1000;
	}

	public int getDailyWoringHours() {
		return 12;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSeniority() {
		return seniority;
	}

	public void setSeniority(int seniority) {
		this.seniority = seniority;
	}
}
