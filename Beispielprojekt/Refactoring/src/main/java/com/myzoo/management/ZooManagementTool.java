package com.myzoo.management;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.myzoo.animals.Lion;
import com.myzoo.animals.Monkey;
import com.myzoo.animals.Snake;
import com.myzoo.animals.Vaccination;
import com.myzoo.animals.VaccinationService;
import com.myzoo.animals.Zoo;
import com.myzoo.employees.Janitor;
import com.myzoo.employees.Manager;

public class ZooManagementTool {

	Zoo zoo = new Zoo();

	private List<Lion> lionStock;
	private List<Monkey> monkeyStock;
	private List<Snake> snakeStock;

	public void setZoo(Zoo zoo) {
		this.zoo = zoo;
		lionStock = zoo.getLionStock();
		monkeyStock = zoo.getMonkeyStock();
		snakeStock = zoo.getSnakeStock();
	}

	public String createOrder() {
		return lionStock.size() + "x " + lionStock.get(0).getFodderType().getLabel() + ", " + monkeyStock.size() + "x "
				+ monkeyStock.get(0).getFodderType().getLabel() + ", " + snakeStock.size() + "x "
				+ snakeStock.get(0).getFodderType().getLabel();
	}

	public List<Vaccination> createVaccinationList() {
		VaccinationService service = new VaccinationService();
		List<Vaccination> vaccinationList = new ArrayList<Vaccination>();
		for (Lion lion : lionStock) {
			LocalDate lastVaccination = service.getLastVaccination(lion.getName());
			vaccinationList.add(new Vaccination(lion.getName(), lion.nextRequiredVaccination(lastVaccination)));
		}

		for (Monkey monkey : monkeyStock) {
			LocalDate lastVaccination = service.getLastVaccination(monkey.getName());
			vaccinationList.add(new Vaccination(monkey.getName(), monkey.nextRequiredVaccination(lastVaccination)));
		}

		for (Snake snake : snakeStock) {
			LocalDate lastVaccination = service.getLastVaccination(snake.getName());
			vaccinationList.add(new Vaccination(snake.getName(), snake.nextRequiredVaccination(lastVaccination)));
		}

		return vaccinationList;
	}

	public void changeManager(Manager manager) {
		zoo.setManager(manager);
	}

	public void hireJanitor(Janitor janitor) {
		zoo.addJanitor(janitor);
	}

	public void fireJanitor(Janitor janitor) {
		zoo.removeJanitor(janitor);
	}

	public int getMonthlySalary(Object employee) {
		if (employee instanceof Janitor) {
			return zoo.getEmployedJanitors().get(0).getMonthlySalary();
		} else if (employee instanceof Manager) {
			return zoo.getManager().getMonthlySalary();
		}
		return 0;

	}

	public double get10PercentBonus(int salary) {
		return salary * 1.1;
	}

	public double get20PercentBonus(int salary) {
		return salary * 1.2;
	}

	public double get50PercentBonus(int salary) {
		return salary * 1.5;
	}

}
